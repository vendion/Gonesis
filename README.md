
[install go]: http://golang.org/install.html "Install Go"
[the godoc url]: http://localhost:6060/pkg/github.com/vendion/Gonesis/ "the Godoc URL"

About Gonesis
=============

Gonesis is a MUD style text based RPG

Documentation
=============

Usage
-----

Run Gonesis with the command

    Gonesis [options]

Prerequisites
-------------

[Install Go][].

Installation
-------------

Use goinstall to install Gonesis

    goinstall github.com/vendion/Gonesis

General Documentation
---------------------

Use godoc to vew the documentation for Gonesis

    godoc github.com/vendion/Gonesis

Or alternatively, use a godoc http server

    godoc -http=:6060

and visit [the Godoc URL][]


Author
======

vendion &lt;vendion@gmail.com&gt;

Copyright & License
===================

Copyright (c) 2012, vendion.
All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.
