// Copyright 2012, vendion. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

/*  Filename:    Gonesis.go
 *  Author:      vendion <vendion@gmail.com>
 *  Created:     Wed Feb  1 16:32:06 EST 2012
 *  Description: A simple MUD style RPG
 */

import (
    //"log"
    "fmt"
    "os"
    "strings"
    "termon.googlecode.com/hg"
)

var opt Options

func init() {
    opt = parseFlags()
}

type Player struct {
    Health, MaxHealth, Level, Mana, MaxMana, tnl int
    Strength, Intelligence, Wisdom, Agility int
    Name, Class, Race string
    Gold, CompletedQuests int
}

type Monster struct {
    X, Y int
}

var p = Player{100, 100, 1, 100, 100, 1000, 20, 20, 25, 30, "null", "null", "null", 100, 0}
var monsters = []Monster{
    {10, 10},
    {20, 5},
    {70, 20},
    {69, 19},
    {50, 5},
}
func main() {
    Start()
    os.Exit(0)
}

func Start() {
    term.Init()
    //Come up with a ASCII logo
    term.Keypad()
    term.HalfDelay(1)
    term.Add("Please tell me who though are? (Enter 'new' to create a new character)\n")
    p.Name = term.GetText()
    if p.Name == "new" {
        CharacterCreation()
    }
    DrawHeader()
    Listen()
    term.End()
}

func CharacterCreation() {
    term.Add("\nWhat shall thou be called?\n")
    p.Name = term.GetText()
    term.Add(fmt.Sprintf("\nWhat type of adventurer are you %s?\n", p.Name))
    term.Add("Fighter\n")
    term.Add("Paliden\n")
    term.Add("Theift\n")
    term.Add("Mage\n")
    p.Class = term.GetText()
    p.Class = strings.ToLower(p.Class)
    switch p.Class {
        case "fighter":
            p.Health += 50
            p.MaxHealth += 50
            p.Strength += 5
            p.Agility += 3
            p.Class = "Fighter"
        case "paliden":
            p.Health += 50
            p.MaxHealth += 50
            p.Mana += 25
            p.MaxMana += 25
            p.Strength += 5
            p.Class = "Paliden"
        case "theift":
            p.Health += 50
            p.MaxHealth += 50
            p.Strength += 5
            p.Agility += 5
            p.Class = "Theif"
        case "mage":
            p.Mana += 50
            p.MaxMana += 50
            p.Intelligence += 5
            p.Wisdom += 5
            p.Agility += 5
            p.Class = "Mage"
        default:
            term.Add(fmt.Sprintf("Sorry %s is not a valid class\n", p.Class))
            CharacterCreation()
    }
    term.Add(fmt.Sprintf("\nWhat race are you %s?\n", p.Name))
    term.Add("Human\n")
    term.Add("Elf\n")
    term.Add("Wood Elf\n")
    term.Add("Dark Elf\n")
    term.Add("Dwarf\n")
    p.Race = term.GetText()
    p.Race = strings.ToLower(p.Race)
    switch p.Race {
        case "human":
            p.Race = "Human"
        case "elf":
            p.Race = "Elf"
        case "wood elf":
            p.Race = "Wood Elf"
        case "dark elf":
            p.Race = "Dark Elf"
        case "dwarf":
            p.Race = "Dwarf"
        default:
            term.Add(fmt.Sprintf("Sorry %s is not a valid race\n", p.Race))
            CharacterCreation()
    }
    term.Add(fmt.Sprintf("\nSo %s, you are a %s that was trained in the ways of a %s\nIs this correct?", p.Name, p.Race, p.Class))
    term.GetChar()
}

func DrawHeader() {
    term.Clear()
    f := "Health: %d/%d\tMana: %d/%d\tLevel: %d\tTNL: %d"
    term.AddAt(0, 0, fmt.Sprintf(f, p.Health, p.MaxHealth, p.Mana, p.MaxMana, p.Level, p.tnl))
}

//May not use but will keep as an example for now...
func Listen() {
    for {
        c := term.GetChar()
        switch c {
            case term.KEY_UP:
                term.Add("Up Key pressed\n")
            case term.KEY_DOWN:
                term.Add("Down Key pressed\n")
            case term.KEY_RIGHT:
                term.Add("Right Key pressed\n")
            case term.KEY_LEFT:
                term.Add("Left Key pressed\n")
        }
    }
    DrawHeader()
}
