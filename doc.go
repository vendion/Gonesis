// Copyright 2012, vendion. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*  Filename:    doc.go
 *  Author:      vendion <vendion@gmail.com>
 *  Created:     Wed Feb  1 16:32:06 EST 2012
 *  Description: Godoc documentation for Gonesis
 */


/*
Gonesis does...

Usage:

    Gonesis [options] ARGUMENT ...

Arguments:

Options:

*/
package documentation
